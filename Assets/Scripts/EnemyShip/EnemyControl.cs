﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class EnemyControl : MonoBehaviour
{
    public GameObject Explosion;
    private GameObject scoreUIText;
    
    private float speed;
    // Start is called before the first frame update
    void Start()
    {
        speed = 3f;

        scoreUIText = GameObject.FindGameObjectWithTag("TextScore");
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 position = transform.position;
        
        position = new Vector2(position.x, position.y - speed * Time.deltaTime);

        transform.position = position;

        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        if (transform.position.y < min.y)
        {
            Destroy(gameObject);
        }
    }
    
    //Detect collision
    private void OnTriggerEnter2D(Collider2D col)
    {
        if ((col.tag == "PlayerShip") || (col.tag == "PlayerBullet"))
        {
            PlayExplosion();
            
            scoreUIText.GetComponent<GameScore>().Score += 100;
            
            Destroy(gameObject);
        }
    }

    void PlayExplosion()
    {
        GameObject explosion = (GameObject) Instantiate(Explosion);

        explosion.transform.position = transform.position;
    }
}
