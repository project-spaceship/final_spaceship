﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float playerShipSpeed = 10;
        private Vector2 movementInput = Vector2.zero;
        public GameObject GameManager;
        public GameObject Explosion;
        
        //Ref UI
        public Text LivesUIText;
        private const int MaxLives = 3;
        private int lives;

        public void Init()
        {
            lives = MaxLives;
            
            LivesUIText.text = lives.ToString();

            transform.position = new Vector2(0, 0);
            
            gameObject.SetActive(true);

        }

        //Player Shoot
        public GameObject PlayerBullet;
        public GameObject BulletPosition;
        
        private float xMin;
        private float xMax;
        private float yMin;
        private float yMax;

        private float padding = 1;

        private void Start()
        {
            SetupMoveBoundaries();
        }

        private void Update()
        {
            Move();
            
            //Shoot Bullet Space
            if (Input.GetKeyDown("space"))
            {
                gameObject.GetComponent<AudioSource>().Play();
                
                GameObject bullet = (GameObject) Instantiate(PlayerBullet);
                bullet.transform.position = BulletPosition.transform.position;
            }
        }

        private void SetupMoveBoundaries()
        {
            Camera gameCamera = Camera.main;
            xMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).x + padding ;
            xMax = gameCamera.ViewportToWorldPoint(new Vector2(1, 0)).x - padding ;

            yMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).y + padding;
            yMax = gameCamera.ViewportToWorldPoint(new Vector2(0, 1)).y - padding;
        }

        private void Move()
        {
            var inputDirection = movementInput.normalized;
            var inPutVelocity = inputDirection * playerShipSpeed;
            
            var gravityForce = new Vector2(0,-2);
            var blackholeForce = new Vector2(2, 0);

            var finalVelocity = inPutVelocity;
            //var finalVelocity = inPutVelocity + gravityForce + blackholeForce ; 
            
            Debug.DrawRay(transform.position, inPutVelocity,Color.cyan);
            Debug.DrawRay(transform.position, gravityForce,Color.red);
            //Debug.DrawRay(transform.position, blackholeForce,Color.yellow);
            Debug.DrawRay(transform.position, finalVelocity,Color.white);

            var newXPos = transform.position.x + finalVelocity.x * Time.deltaTime;
            var newYPos = transform.position.y + finalVelocity.y * Time.deltaTime;

            newXPos = Mathf.Clamp(newXPos, xMin, xMax);
            newYPos = Mathf.Clamp(newYPos, yMin, yMax);
            
            transform.position = new Vector2(newXPos, newYPos);
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            movementInput = context.ReadValue<Vector2>();
        }

        //Detect collision 
        private void OnTriggerEnter2D(Collider2D col)
        {
            if ((col.tag == "EnemyShip") || (col.tag == "EnemyBullet") || (col.tag == "MeteorTag"))
            {
                PlayExplosion();
                
                lives--;
                LivesUIText.text = lives.ToString();

                if (lives == 0)
                {
                    //Destroy(gameObject);
                    GameManager.GetComponent<GameManager>().SetGameManagerState(global::GameManager.GameManagerState.GameOver);
                    
                    gameObject.SetActive(false);

                }
            }
        }

        void PlayExplosion()
        {
            GameObject explosion = (GameObject) Instantiate(Explosion);

            explosion.transform.position = transform.position;
        }
    }
}
