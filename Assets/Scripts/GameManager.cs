﻿using System;
using System.Collections;
using System.Collections.Generic;
using PlayerShip;
using UnityEditor.Rendering;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject playButton;
    public GameObject playerShip;
    public GameObject enemySpawner;
    public GameObject meteorSpawner;
    public GameObject GameOverUI;
    public GameObject scoreUIText;
    public GameObject TimeCounter;
    
    public enum GameManagerState
    {
        Opening,
        Gameplay,
        GameOver,
    }
    
    GameManagerState GMState;
    
    // Start is called before the first frame update
    void Start()
    {
        GMState = GameManagerState.Opening;
    }

    void UpdateGameManagerState()
    {
        switch (GMState)
        {
            case GameManagerState.Opening:
                
                //Hide gameover
                GameOverUI.SetActive(false);
                
                playButton.SetActive(true);
                
                
                break;
            case GameManagerState.Gameplay:

                //reset score
                scoreUIText.GetComponent<GameScore>().Score = 0;
                
                //hide play button
                playButton.SetActive(false);
                
                playerShip.GetComponent<PlayerController>().Init();
                
                enemySpawner.GetComponent<EnemySpawner>().ScheduleEnemySpawner();
                
                meteorSpawner.GetComponent<MeteorSpawner>().ScheduleMeteorSpawner();
                
                TimeCounter.GetComponent<TimeCounter>().StartTimeCounter();
                
                break;
            case GameManagerState.GameOver:
                
                TimeCounter.GetComponent<TimeCounter>().StopTimeCounter();
                
                enemySpawner.GetComponent<EnemySpawner>().UnscheduleEnemySpawner();
                
                meteorSpawner.GetComponent<MeteorSpawner>().UnscheduleMeteorSpawner();
                
                GameOverUI.SetActive(true);

                Invoke("ChangeToOpeningState",8f);
                
                break;
        }
    }

    public void SetGameManagerState(GameManagerState state)
    {
        GMState = state;
        UpdateGameManagerState();
    }

    public void StartGamePLay()
    {
        GMState = GameManagerState.Gameplay;
        UpdateGameManagerState();
    }

    public void ChangeToOpeningState()
    {
        SetGameManagerState(GameManagerState.Opening);
    }

}
