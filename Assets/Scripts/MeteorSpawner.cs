﻿using System.Collections;
using System.Collections.Generic;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEngine;

public class MeteorSpawner : MonoBehaviour
{
    public GameObject Meteor;

    private float maxSpawnRateInSeconds = 60f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnEnemy()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        GameObject anEnemy = (GameObject) Instantiate(Meteor);
        anEnemy.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);

        ScheduleNextEnemySpawn();
    }

    void ScheduleNextEnemySpawn()
    {
        float spawnInNSeconds;

        if (maxSpawnRateInSeconds > 1f)
        {
            spawnInNSeconds = Random.Range(1f, maxSpawnRateInSeconds);
        }
        else
        {
            spawnInNSeconds = 1f;
        }
        Invoke("SpawnEnemy",spawnInNSeconds);
    }

    void IncressSpawnRate()
    {
        if (maxSpawnRateInSeconds > 1f)
            maxSpawnRateInSeconds--;
        
        if (maxSpawnRateInSeconds == 1f)
            CancelInvoke("IncressSpawnRate");
    }

    public void ScheduleMeteorSpawner()
    {
        Invoke("SpawnEnemy", maxSpawnRateInSeconds);
        
        InvokeRepeating("IncressSpawnRate",20f,30f);
    }

    public void UnscheduleMeteorSpawner()
    {
        CancelInvoke("SpawnEnemy");
        CancelInvoke("IncressSpawnRate");
    }
}
